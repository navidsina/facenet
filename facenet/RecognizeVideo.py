from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import pickle
import time
import sys
import cv2
import numpy as np
import tensorflow as tf
from scipy import misc

import facenet
from align import detect_face


def main(inputVideosPath, modelDir, classifierFileName, rawImagesDirectoryForNameExtraction, twoCorners, namesInGrid):
      
    modeldir = modelDir #'C:/Media/Downloads/20180402-114759/20180402-114759/'
    classifier_filename = classifierFileName #'C:/Media/Downloads/20180402-114759/20180402-114759/my_classifier.pkl'
    npy=''
    train_img = rawImagesDirectoryForNameExtraction #"C:/Media/Downloads/Face_ID-master/Face_ID-master/facenet/dataset/raw"
    with tf.Graph().as_default():
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.6)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
        with sess.as_default():
            pnet, rnet, onet = detect_face.create_mtcnn(sess, npy)

            minsize = 20  # minimum size of face
            threshold = [0.8, 0.8, 0.8]  # three steps's threshold
            factor = 0.709  # scale factor
            margin = 32
            frame_interval = 3
            batch_size = 1000
            image_size = 160
            input_image_size = 160

            HumanNames = os.listdir(train_img)
            HumanNames.sort()

            print('Loading Modal')
            facenet.load_model(modeldir)
            images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")
            embedding_size = embeddings.get_shape()[1]


            classifier_filename_exp = os.path.expanduser(classifier_filename)
            with open(classifier_filename_exp, 'rb') as infile:
                (model, class_names) = pickle.load(infile)

            outputFile = open("numberOfRecognisedFacesInVideos.txt", "a")              
            inputImageDirectoryIndex = 0

            onlyfiles = [f for f in os.listdir(inputVideosPath) if os.path.isfile(os.path.join(inputVideosPath, f))]
            for input_video in onlyfiles:
                numberOfRecognizedFaces = []
                twoCornersForCells = []                   
                for i in range(len(namesInGrid)):
                    numberOfRecognizedFaces.append(0)
                    indexY = int(i / 4)
                    indexX = i % 4
                    if(len(twoCorners) != 0):
                        cellX0 = twoCorners[inputImageDirectoryIndex][0][0] + indexX * (abs(twoCorners[inputImageDirectoryIndex][0][0] - twoCorners[inputImageDirectoryIndex][1][0])/4)
                        cellY0 = twoCorners[inputImageDirectoryIndex][0][1] + indexY * (abs(twoCorners[inputImageDirectoryIndex][0][1] - twoCorners[inputImageDirectoryIndex][1][1])/4)
                        cellX1 = cellX0 + (abs(twoCorners[inputImageDirectoryIndex][0][0] - twoCorners[inputImageDirectoryIndex][1][0])/4)
                        cellY1 = cellY0 + (abs(twoCorners[inputImageDirectoryIndex][0][1] - twoCorners[inputImageDirectoryIndex][1][1])/4)
                        twoCornersForCells.append([[cellX0, cellY0],[cellX1, cellY1]])
                video_capture = cv2.VideoCapture(inputVideosPath + input_video)
                width = int(video_capture.get(cv2.CAP_PROP_FRAME_WIDTH))   # float
                height = int(video_capture.get(cv2.CAP_PROP_FRAME_HEIGHT)) # float
                fourcc = cv2.VideoWriter_fourcc(*'MP4V')
                outputDIR = 'output'
                out = cv2.VideoWriter(outputDIR,fourcc, 25.0, (width,height))
                c = 0

                
                print('Start Recognition')
                prevTime = 0
                while True:
                    ret, frame = video_capture.read()
                    if ret == False:
                        break
                   
                    #frame = cv2.resize(frame, (0,0), fx=0.5, fy=0.5)    #resize frame (optional)

                    curTime = time.time()+1    # calc fps
                    timeF = frame_interval

                    if (c % timeF == 0):
                        find_results = []

                        if frame.ndim == 2:
                            frame = facenet.to_rgb(frame)
                        frame = frame[:, :, 0:3]
                        bounding_boxes, _ = detect_face.detect_face(frame, minsize, pnet, rnet, onet, threshold, factor)
                        nrof_faces = bounding_boxes.shape[0]
                        print('Detected_FaceNum: %d' % nrof_faces)

                        if nrof_faces > 0:
                            det = bounding_boxes[:, 0:4]
                            img_size = np.asarray(frame.shape)[0:2]

                            cropped = []
                            scaled = []
                            scaled_reshape = []
                            bb = np.zeros((nrof_faces,4), dtype=np.int32)

                            for i in range(nrof_faces):
                                emb_array = np.zeros((1, embedding_size))

                                bb[i][0] = det[i][0]
                                bb[i][1] = det[i][1]
                                bb[i][2] = det[i][2]
                                bb[i][3] = det[i][3]

                                # inner exception
                                if bb[i][0] <= 0 or bb[i][1] <= 0 or bb[i][2] >= len(frame[0]) or bb[i][3] >= len(frame):
                                    print('Face is very close!')
                                    #continue

                                cropped.append(frame[bb[i][1]:bb[i][3], bb[i][0]:bb[i][2], :])
                                cropped[i] = facenet.flip(cropped[i], False)
                                scaled.append(misc.imresize(cropped[i], (image_size, image_size), interp='bilinear'))
                                scaled[i] = cv2.resize(scaled[i], (input_image_size,input_image_size),
                                                        interpolation=cv2.INTER_CUBIC)
                                scaled[i] = facenet.prewhiten(scaled[i])
                                scaled_reshape.append(scaled[i].reshape(-1,input_image_size,input_image_size,3))
                                feed_dict = {images_placeholder: scaled_reshape[i], phase_train_placeholder: False}
                                emb_array[0, :] = sess.run(embeddings, feed_dict=feed_dict)
                                predictions = model.predict_proba(emb_array)
                                print(predictions)
                                best_class_indices = np.argmax(predictions, axis=1)
                                best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]
                                # print("predictions")
                                print(best_class_indices,' with accuracy ',best_class_probabilities)

                                cv2.rectangle(frame, (bb[i][0], bb[i][1]), (bb[i][2], bb[i][3]), (0, 255, 0), 2)    #boxing face
                                # print(best_class_probabilities)
                                if best_class_probabilities>0.40:

                                    #plot result idx under box
                                    text_x = bb[i][0]
                                    text_y = bb[i][3] + 20
                                    print('Result Indices: ', best_class_indices[0])
                                    print(HumanNames)
                                    for H_i in HumanNames:
                                        if HumanNames[best_class_indices[0]] == H_i:
                                            result_names = HumanNames[best_class_indices[0]]
                                            cv2.putText(frame, result_names, (text_x, text_y), cv2.FONT_HERSHEY_COMPLEX_SMALL, 2, (0, 0, 255), thickness=2, lineType=2)                                            
                                            if(result_names in namesInGrid):
                                                indexInGrid = namesInGrid.index(result_names)
                                                midX = (bb[i][0] + bb[i][2]) / 2
                                                midY = (bb[i][1] + bb[i][3]) / 2                                            
                                                if(len(twoCorners) != 0):
                                                    if(twoCornersForCells[indexInGrid][0][0] <= midX and midX <= twoCornersForCells[indexInGrid][1][0] and twoCornersForCells[indexInGrid][0][1] <= midY and midY <= twoCornersForCells[indexInGrid][1][1]):
                                                        numberOfRecognizedFaces[indexInGrid] += 1
                                                else:
                                                    numberOfRecognizedFaces[indexInGrid] += 1

                        else:
                            print('Alignment Failure')
                    # c+=1
                    #out.write(frame)
                    cv2.imshow("im", frame)
                    cv2.setWindowTitle("im", input_video)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break

                video_capture.release()
                #out.release()
                for i in range(len(namesInGrid)):
                    printValue = "number of recognized faces for subject " + str(i) + " in " + input_video + ": " + str(numberOfRecognizedFaces[i])
                    print(printValue)
                    outputFile.write(printValue + "\n")
        cv2.destroyAllWindows()
