import cv2
import os
from os import listdir
from os.path import isfile, join


def ConvertVideoToImages(TrainingPath, SavingRawPath, SkipFactor):
    trainingPath = TrainingPath#"E:\\honda\\training\\"
    SavingRawPath = SavingRawPath#"E:\\honda\\raw\\"
    skipFactor = SkipFactor#20
    onlyfiles = [f for f in listdir(trainingPath) if isfile(join(trainingPath, f))]
    for trainingVideo in onlyfiles:
         videoCapture = cv2.VideoCapture(trainingPath + trainingVideo)
         savingPath = SavingRawPath + trainingVideo[:trainingVideo.index(".")]
         if not os.path.exists(savingPath):
            os.makedirs(savingPath)
         frameNumber = 0
         while(True):
            ret, frame = videoCapture.read()
            if ret == False:
                break
            cv2.imshow("image", frame);
            cv2.waitKey(1)
            frameNumber += 1
            if(frameNumber%skipFactor == 0):
                cv2.imwrite(savingPath + "\\" + str(frameNumber) + '.png',frame)

