import cv2
import random
from os import listdir
from os.path import isfile, join

def faceDetection(listOfInputImageDirectories):

    # Load the cascade
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_alt2.xml')
    for inputImageDirectory in listOfInputImageDirectories:
        onlyImagefiles = [f for f in listdir(inputImageDirectory) if (isfile(join(inputImageDirectory, f)) and (f.split(".")[-1] == "ppm") )]
        counter = 1
        numberOfDetectedFaces = 0
        random.shuffle(onlyImagefiles)
        k = cv2.waitKey(0)
        for img_name in onlyImagefiles[:500]:
            img_path = join(inputImageDirectory, img_name)
            img = cv2.imread(img_path,0)            

            gray = img
            # Detect the faces
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)
            numberOfDetectedFaces += len(faces)
            # Draw the rectangle around each face
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            for (x, y, w, h) in faces:
                cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
            # Display
            cv2.imshow('im', img)
            cv2.setWindowTitle("im", img_path + " " + str(counter) + " " + str(numberOfDetectedFaces))
            counter += 1
            # Stop if escape key is pressed
            waitValue = 0 if len(faces) > 0 else 1
            k = cv2.waitKey(waitValue) & 0xff
            if k==27:
                break
