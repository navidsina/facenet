import csv
import os
import shutil
from random import random
from PIL import Image
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

def csvFileToRawImages():
    with open('D:\\WORK\\IJB-C Dataset\\IJB\IJB-B\\protocol\\ijbb_1N_probe_video.csv') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')        
        line_count = 0
        personCount = 0
        trainVideosFile = open("E:\\honda\\rawForJanus\\TrainVideos.txt","w")
        for row in csv_reader:               
            if personCount == 20000:
                break
            if line_count == 0:
                print(f'Column names are {", ".join(row)}')
                line_count += 1
            else:
                if random() > 0.9:
                    trainVideosFile.write(str(row).strip('[]') + "\n")
                    continue         
                rawDirectory = "E:\\honda\\rawForJanus\\" + row[1]
                if not os.path.exists(rawDirectory):
                    os.makedirs(rawDirectory)
                    personCount += 1                
                if(row[2].split("/")[0] == "frame"):
                    row[2] = "frames/" + row[2].split("/")[-1]
                source = "D:\\WORK\\IJB-C Dataset\\IJB\\IJB-B\\images" + "\\" + "\\".join(row[2].split("/"))
               
                im = Image.open(source) 
                width, height = im.size 
                left = int(float(row[5]))
                top = int(float(row[6]))
                right = left + int(float(row[7]))
                bottom = top + int(float(row[8]))
                im1 = im.crop((left, top, right, bottom))

                destination = rawDirectory + "\\" + str(personCount) + "-" + row[2].split("/")[-1]                
  
                # Copy the content of 
                # source to destination
                im1.save(destination) 
                im.close()
                #shutil.copyfile(source, destination)

                print(f'\t{row[0]}   {row[1]}   {row[2]}.')
                line_count += 1
        print(f'Processed {line_count} lines.')
        trainVideosFile.close()

        with open('D:\\WORK\\IJB-C Dataset\\IJB\\IJB-A\\IJB-A_1N_sets\\split2\\train_2.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')        
            line_count = 0            
            for row in csv_reader:               
                if line_count == 0:
                    print(f'Column names are {", ".join(row)}')
                    line_count += 1
                else:                    
                    testDirectory = "E:\\honda\\testForJanus\\" + row[1]
                    if not os.path.exists(testDirectory):
                        os.makedirs(testDirectory)
                    if(row[2].split("/")[0] == "frame"):
                        row[2] = "frames/" + row[2].split("/")[-1]
                    source = "D:\\WORK\\IJB-C Dataset\\IJB\\IJB-A\\images" + "\\" + "\\".join(row[2].split("/"))
               
                    im = Image.open(source) 
                    width, height = im.size 
                    left = int(float(row[6]))
                    top = int(float(row[7]))
                    right = left + int(float(row[8]))
                    bottom = top + int(float(row[9]))
                    im1 = im.crop((left, top, right, bottom))
                    destination = testDirectory + "\\" + str(personCount) + "-" + row[2].split("/")[-1]
  
                    # Copy the content of 
                    # source to destination
                    im1.save(destination) 
                    im.close()
                    #shutil.copyfile(source, destination)

                    print(f'\t{row[0]}   {row[1]}   {row[2]}.')
                    line_count += 1
            print(f'Processed {line_count} lines.')
